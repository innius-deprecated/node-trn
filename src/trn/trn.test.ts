import {assert} from "chai"
import * as trn from "./trn"

describe("TRN", () => {
    describe("#ParseTRN", () => {
        let ts = "trn:123456:machine:234234234/collateral/document1.pdf"
        // = trn.TRN.Parse(ts);
        let t = trn.TRN.parse(ts);
        it("trn should have expected company id", () => {
            assert.equal("123456", t.company);
        });
        it("trn sould have expected ResourceType", () => {
            assert.equal(trn.ResourceType.machine, t.resourceType);
        })
        it("trn should have expected resource id", () => {
            assert.equal("234234234", t.resourceID);
        })
        it("trn should have expected sub resource type", () => {
            assert.equal("collateral", t.subResourceType);
        })
        it("trn should have expected sub resource id", () => {
            assert.equal("document1.pdf", t.subResourceID);
        })
    });

    describe("#ToString", () => {
        var ptrn = new trn.TRN("123456", trn.ResourceType.machine, "456789")
        it("should be a valid trn string", () => {
            assert.equal("trn:123456:machine:456789", ptrn.toString())
        })        
    })

    describe("#toString with subresource", () => {
        var ptrn = new trn.TRN("123456", trn.ResourceType.machine, "456789");
        ptrn.subResourceType = "specifications";
        ptrn.subResourceID = "electrical";
        it("should have the expected value", () => {
            assert.equal(ptrn.toString(), "trn:123456:machine:456789/specifications/electrical")
        })
    })

    describe("#owns", () => {
        let ptrn = new trn.TRN("acme.com", trn.ResourceType.machine, "1234");
        assert.isTrue(trn.TRN.Owns("acme.com", ptrn.toString()));                    
    })
});
