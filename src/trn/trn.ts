class InvalidFormatError extends Error {
    constructor(strn: string) {
        super(`${strn} is not valid; format of a trn string is trn:<companyid>:<resourcetype>:<resourceid>`);
    }
}

/**
 * A TRN is a global unique identifier for addable resources; it has a fixed format which comprises the 
 * following elements: 
 *  - company           : the company who owns the resource
 *  - resourceType      : the type of the resource, for example: machine 
 *  - resourceID        : the uuid of the resource 
 *  - subResourceType   : optional sub resource, for example collateral 
 *  - subResourceID     : the uuid of the sub resource
 * 
 * @export
 * @class TRN
 */
export class TRN {
    public subResourceType: string;
    public subResourceID: string;
    constructor(
        public company: Identifier,
        public resourceType: ResourceType,
        public resourceID: Identifier
    ) { }

    /**
     * parses a string into a trn
     * 
     * @static
     * @param {string} strn the string in trn format
     * @throws {InvalidFormatError}     
     * @returns {TRN} 
     */
    static parse(strn: string): TRN {
        let trns = strn.split(":");
        if ((trns.length !== 4) || (trns[0] !== "trn")) {
            throw new InvalidFormatError(strn);
        }
        let resourceType = ResourceType[trns[2]];
        if (resourceType === undefined) {
            throw new Error(`resource type ${trns[2]} is not supported`);
        }

        // parse the sub resource         
        let rp = trns[3].split("/", 3);
        let ptrn = new TRN(trns[1], resourceType, rp[0]);
        if (rp.length === 3) {
            ptrn.subResourceType = rp[1];
            ptrn.subResourceID = rp[2];
        } else if (rp.length === 2) {
            ptrn.subResourceType = rp[1];
        }

        return ptrn;
    }

    public toString(): string {
        let result = `trn:${this.company}:${ResourceType[this.resourceType]}:${this.resourceID}`;
        if (this.subResourceType) {
            result += `/${this.subResourceType}`;
        }
        if (this.subResourceID) {
            result += `/${this.subResourceID}`;
        }
        return result;
    }

    /**
     * checks if trn is owned by a company, by comparing the company part of the trn
     * @returns true if company matches 
     */
    public ownedBy(company: string): boolean {
        return this.company === company;
    }

    /**
     * checks if trn is owned by a company by comparing the company part of the trn 
     * @param {string} company the company uuid 
     * @param {string} id a valid trn string 
     * @returns true if company matches
     */
    static Owns(company: string, id: string): boolean {
        let trn = TRN.parse(id);
        return trn.ownedBy(company);
    }
}

export type Identifier = string;

export enum ResourceType {
    machine = 1,
    company,
    person
}
