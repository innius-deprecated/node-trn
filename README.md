# node-trn

To-Increase Resource Names (TRNs) uniquely identify our resources. We require an TRN when you need to specify a resource unambiguously across our platform, such as in Machines, Companies and Persons.

## TRN Format
Here are some sample TRNs:
```
<!-- Machine -->
trn:b9066d9f-0aa9-4684-9edb-e5c99ca145cf:machine:b9066d9f-0aa9-4684-9edb-e5c99ca145er
<!-- Company -->
trn:b9066d9f-0aa9-4684-9edb-e5c99ca145cf:company:b9066d9f-0aa9-4684-9edb-e5c99ca145er
<!-- Person -->
trn:b9066d9f-0aa9-4684-9edb-e5c99ca145cf:Person:b9066d9f-0aa9-4684-9edb-e5c99ca145er
```

The following are the general formats for TRNs; the specific components and values used depend on the resource type.
```
trn:company-id:resourcetype:resource-id
trn:company-id:resourcetype:resource-id/subtype
```
### company-id
The company that owns the resource. 

### resourcetype
The type of the resource. Currently the following types are supported:
* machine
* company
* person

### resource-id
the unique id of the resource. 

### subtype
The content of this part of the TRN varies by resource type. 
examples: 
```
trn:b9066d9f-0aa9-4684-9edb-e5c99ca145cf:machine:b9066d9f-0aa9-4684-9edb-e5c99ca145er/specs/electrical
```